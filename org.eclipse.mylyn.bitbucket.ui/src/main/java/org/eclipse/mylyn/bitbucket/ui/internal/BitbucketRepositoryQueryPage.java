package org.eclipse.mylyn.bitbucket.ui.internal;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.mylyn.bitbucket.internal.Bitbucket;
import org.eclipse.mylyn.bitbucket.internal.BitbucketAuthorizationException;
import org.eclipse.mylyn.bitbucket.internal.BitbucketService;
import org.eclipse.mylyn.bitbucket.internal.BitbucketServiceException;
import org.eclipse.mylyn.bitbucket.internal.model.BBComponents;
import org.eclipse.mylyn.bitbucket.internal.model.BBKind;
import org.eclipse.mylyn.bitbucket.internal.model.BBMilestones;
import org.eclipse.mylyn.bitbucket.internal.model.BBPriority;
import org.eclipse.mylyn.bitbucket.internal.model.BBStatus;
import org.eclipse.mylyn.bitbucket.internal.model.BBVersions;
import org.eclipse.mylyn.tasks.core.IRepositoryQuery;
import org.eclipse.mylyn.tasks.core.TaskRepository;
import org.eclipse.mylyn.tasks.ui.wizards.AbstractRepositoryQueryPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Text;

/**
 * This backs the page where you create a repository query
 * We do some voodoo here. Mylyn has a Map backing url params (i.e. one at a time) so we
 * do csv's. This then pops up in @BitbucketQuery
 * @author shuji.w6e
 * @since 0.1.0
 */
public class BitbucketRepositoryQueryPage extends AbstractRepositoryQueryPage {

	private List statusList;
    private List kindList;
    private Text queryTitleText;
    private List versionList;
    private List milestoneList;
    private List componentList;
    private List priorityList;
    private Text titleText;
    private Text assigneeText;
    
    BitbucketService bbs;

    private final ModifyListener modifyListenr = new ModifyListener() {
        public void modifyText(ModifyEvent e) {
            if (isControlCreated()) {
                setPageComplete(isPageComplete());
            }
        }
    };

    /**
     * Constructor.
     * @param taskRepository
     * @param query
     */
    public BitbucketRepositoryQueryPage(TaskRepository taskRepository, IRepositoryQuery query) {
      super("Bitbucket", taskRepository, query);
      setTitle("Bitbucket search query parameters");
      setDescription("Enter query title and parameters.");
      setImageDescriptor(BitbucketImages.getIcon());
      setPageComplete(false);
      bbs = BitbucketService.get(taskRepository);
    }

    @Override
    public boolean isPageComplete() {
        if (getQueryTitle().isEmpty()) return false;
        setErrorMessage(null);
        return true;
    }

    @Override
    public String getQueryTitle() {
        return queryTitleText.getText();
    }
    
    @Override
    public void applyTo(IRepositoryQuery query) {
        query.setSummary(getQueryTitle());
        
        setAttributeIfNotEmpty(query, Bitbucket.QUERY_KEY_STATUS, statusList.getSelection());
        setAttributeIfNotEmpty(query, Bitbucket.QUERY_KEY_KIND, kindList.getSelection());
        setAttributeIfNotEmpty(query, Bitbucket.QUERY_KEY_VERSION, versionList.getSelection());
        setAttributeIfNotEmpty(query, Bitbucket.QUERY_KEY_MILESTONE, milestoneList.getSelection());
        setAttributeIfNotEmpty(query, Bitbucket.QUERY_KEY_COMPONENT, componentList.getSelection());
        setAttributeIfNotEmpty(query, Bitbucket.QUERY_KEY_PRIORITY, priorityList.getSelection());
        query.setAttribute(Bitbucket.QUERY_KEY_TITLE, titleText.getText());
        query.setAttribute(Bitbucket.QUERY_KEY_ASSIGNEE, assigneeText.getText());
    }
    
    private void setAttributeIfNotEmpty(IRepositoryQuery query, String attribute, String[] params) {
    	if (params.length == 0) return;
        StringBuilder value = new StringBuilder();
        for (String v : params) {
            value.append(v).append(",");
        }
        value.setLength(value.length() - ",".length());
        query.setAttribute(attribute, value.toString());
    }
    
    public void createControl(Composite parent) {
      try {
    	System.err.print("Start of query");
        Composite composite = new Composite(parent, SWT.NONE);
        GridLayoutFactory.swtDefaults().applyTo(composite);
        createGeneralControl(composite);

        if (!bbs.verifyCredentials()) {
          MessageDialog.openError(getShell(), "Not logged in", "Please verify your credentials");
          return;
        }

        createIssueAttributesControl(composite);

        setControl(composite);
      } catch (BitbucketAuthorizationException e) {
        MessageDialog.openError(getShell(), "Not logged in", "Please verify your credentials");
        return;
      } catch (BitbucketServiceException e) {
        throw new RuntimeException(e);//not sure what else to do superclass doesnt handle dynamic lookups
		  }
    }

    private Composite createGeneralControl(Composite parent) {
        Composite group = new Composite(parent, SWT.NONE);
        GridLayoutFactory.swtDefaults().numColumns(2).applyTo(group);
        Label label = new Label(group, SWT.LEFT);
        label.setText("Query Title:");
        queryTitleText = new Text(group, SWT.BORDER);
        if (getQuery() != null) { 
          queryTitleText.setText(getQuery().getSummary());
        }
        queryTitleText.addModifyListener(modifyListenr);
        GridDataFactory.defaultsFor(queryTitleText).applyTo(queryTitleText);
        GridDataFactory.fillDefaults().grab(true, false).applyTo(group);
        return group;
    }

    private Composite createIssueAttributesControl(Composite parent) throws BitbucketServiceException {
        IRepositoryQuery oldQuery = getQuery();
        String[] kinds = BBKind.asArray();
        String[] statuses = BBStatus.asArray();
        String[] priorities = BBPriority.asArray();
        BBVersions vsc =bbs.doGetList(new BBVersions());
        String[] versions = new String[vsc.getList().size()];
        for (int i = 0; i < vsc.getList().size(); i++) {
        	versions[i] = vsc.getList().get(i).getName();
        }
        BBMilestones msc =bbs.doGetList(new BBMilestones());
        String[] milestones = new String[msc.getList().size()];
        for (int i = 0; i < msc.getList().size(); i++) {
        	milestones[i] = msc.getList().get(i).getName();
        }
        BBComponents csc = bbs.doGetList(new BBComponents());
        String[] components = new String[csc.getList().size()];
        for (int i = 0; i < csc.getList().size(); i++) {
        	components[i] = csc.getList().get(i).getName();
        }
        Composite titleGroup = new Composite(parent, SWT.NONE);
        GridLayoutFactory.swtDefaults().numColumns(2).applyTo(titleGroup);
        {
        	Label label = new Label(titleGroup,SWT.LEFT);
        	label.setText("Title");
        }
        {
        	titleText = new Text(titleGroup, SWT.BORDER);
        	if (oldQuery != null) {
        		titleText.setText(oldQuery.getAttribute(Bitbucket.QUERY_KEY_TITLE));
        	}
        }
        {
        	Label label = new Label(titleGroup,SWT.LEFT);
        	label.setText("Assignee");
        }
        {
        	assigneeText = new Text(titleGroup, SWT.BORDER);
        	if (oldQuery != null) {
        		titleText.setText(oldQuery.getAttribute(Bitbucket.QUERY_KEY_ASSIGNEE));
        	}
        }
        Composite group = new Composite(parent, SWT.NONE);
        GridLayoutFactory.swtDefaults().numColumns(6).applyTo(group);
        { // Label
            Label label = new Label(group, SWT.LEFT);
            label.setText("Type");
        }
        {
            Label label = new Label(group, SWT.LEFT);
            label.setText("Priority");
        }
        {
            Label label = new Label(group, SWT.LEFT);
            label.setText("Status");
        }
        {
            Label label = new Label(group, SWT.LEFT);
            label.setText("Version");
        }
        {
            Label label = new Label(group, SWT.LEFT);
            label.setText("Milestone");
        }
        {
            Label label = new Label(group, SWT.LEFT);
            label.setText("Component");
        }
        {
            kindList = new List(group, SWT.MULTI | SWT.BORDER | SWT.V_SCROLL);
            GridDataFactory.defaultsFor(kindList).applyTo(kindList);
            for (String text : kinds) {
                kindList.add(text);
            }
            if (oldQuery != null) {
                String kindValues = oldQuery.getAttribute(Bitbucket.QUERY_KEY_KIND);
                for (int i = 0; i < kinds.length; i++) {
                    if (kindValues.contains(kinds[i])) {
                        kindList.select(i);
                    }
                }
            } else {
                kindList.select(new int[] { 0, 1 });
            }
        }
        {
        	priorityList = new List(group, SWT.MULTI | SWT.BORDER | SWT.V_SCROLL);
            GridDataFactory.defaultsFor(priorityList).applyTo(priorityList);
            for (String text : priorities) {
            	priorityList.add(text);
            }
            if (oldQuery != null) {
                String priorityValues = oldQuery.getAttribute(Bitbucket.QUERY_KEY_PRIORITY);
                if (priorityValues != null) {
                	for (int i = 0; i < priorities.length; i++) {
                	     if (priorityValues.contains(priorities[i])) {
                	       	priorityList.select(i);
                	     }
                	}
                 } else {
                	priorityList.selectAll();
                 }
            } else {
            	priorityList.select(new int[] { 0 });
            }
        }
        {
            statusList = new List(group, SWT.MULTI | SWT.BORDER | SWT.V_SCROLL);
            GridDataFactory.defaultsFor(statusList).applyTo(statusList);
            for (String text : statuses) {
                statusList.add(text);
            }
            if (oldQuery != null) {
                String statusValues = oldQuery.getAttribute(Bitbucket.QUERY_KEY_STATUS);
                for (int i = 0; i < statuses.length; i++) {
                    if (statusValues.contains(statuses[i])) {
                        statusList.select(i);
                    }
                }
            } else {
                statusList.select(new int[] { 0 });
            }
        }
        {
            versionList = new List(group, SWT.MULTI | SWT.BORDER | SWT.V_SCROLL);
            GridDataFactory.defaultsFor(versionList).applyTo(versionList);
            for (String text : versions) {
            	versionList.add(text);
            }
            if (oldQuery != null) {
                String versionValues = oldQuery.getAttribute(Bitbucket.QUERY_KEY_VERSION);
                for (int i = 0; i < versions.length; i++) {
                    if (versionValues.contains(versions[i])) {
                    	versionList.select(i);
                    }
                }
            }
        }
        {
            milestoneList = new List(group, SWT.MULTI | SWT.BORDER | SWT.V_SCROLL);
            GridDataFactory.defaultsFor(milestoneList).applyTo(milestoneList);
            for (String text : milestones) {
            	milestoneList.add(text);
            }
            if (oldQuery != null) {
                String milestoneValues = oldQuery.getAttribute(Bitbucket.QUERY_KEY_MILESTONE);
                for (int i = 0; i < milestones.length; i++) {
                    if (milestoneValues.contains(milestones[i])) {
                    	milestoneList.select(i);
                    }
                }
            }
        }
        {
            componentList = new List(group, SWT.MULTI | SWT.BORDER | SWT.V_SCROLL);
            GridDataFactory.defaultsFor(componentList).applyTo(componentList);
            for (String text : components) {
            	componentList.add(text);
            }
            if (oldQuery != null) {
                String componentValues = oldQuery.getAttribute(Bitbucket.QUERY_KEY_COMPONENT);
                for (int i = 0; i < components.length; i++) {
                    if (componentValues.contains(components[i])) {
                    	componentList.select(i);
                    }
                }
            }
        }
        return group;
    }

}
