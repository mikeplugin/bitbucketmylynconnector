package org.eclipse.mylyn.bitbucket.ui.internal;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

/**
 * 
 * This is a wrapper for the eclipse status mechanisms
 * @author shuji.w6e, msduk
 * @since 0.1.0
 * 
 */
public class BitbucketUiStatus extends Status {
    public static final String BUNDLE_ID = "org.eclipse.mylyn.bitbucket.ui";

    public BitbucketUiStatus(int severity, String message, Throwable exception) {
        super(severity, BUNDLE_ID, message, exception);
    }

    public BitbucketUiStatus(int severity, String message) {
        super(severity, BUNDLE_ID, message);
    }

    public static IStatus newErrorStatus(String message) {
        return new BitbucketUiStatus(IStatus.ERROR, message);
    }

    public static IStatus newErrorStatus(Throwable t) {
        return new BitbucketUiStatus(IStatus.ERROR, t.getMessage(), t);
    }
    
    public static IStatus newErrorStatus(String message, Throwable t) {
        return new BitbucketUiStatus(IStatus.ERROR, message, t);
    }

}
