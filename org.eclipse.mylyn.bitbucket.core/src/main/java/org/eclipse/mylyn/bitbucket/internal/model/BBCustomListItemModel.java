package org.eclipse.mylyn.bitbucket.internal.model;

import java.util.List;

import org.eclipse.mylyn.bitbucket.internal.BitbucketRepository;
import org.json.JSONException;
import org.json.JSONObject;

public class BBCustomListItemModel implements BBModelI {
    protected int size;
    protected int page;
    protected int pagelen;
    protected String next;
    protected String previous;

    public String getName() {;
    return null;
    }

 
    @Override
    public String buildUrl(BitbucketRepository bbr) {
        // Will be overridden
        return null;
    }

    @Override
    public String getKey() {
        // Will be overridden
        return null;
    }


    @Override
    public <T extends BBModelI> List<T> getList() {
        // Will be overridden
        return null;
    }

    @Override
    public BBModelI interpretReturn(JSONObject object) throws JSONException {
        // Will be overridden
        return null;
    }

    @Override
    public String generateJson() {
        // Will be overridden
        return null;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int sizep) {
        size = sizep;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int pagep) {
        page = pagep;
    }

    public int getPagelen() {
        return pagelen;
    }

    public void setPagelen(int pagelenp) {
        pagelen = pagelenp;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String nextp) {
        next = nextp;
    }

    public String getPrevious() {
        return previous;
    }

    public void setPrevious(String previousp) {
        previous = previousp;
    }
    
    
    
}
