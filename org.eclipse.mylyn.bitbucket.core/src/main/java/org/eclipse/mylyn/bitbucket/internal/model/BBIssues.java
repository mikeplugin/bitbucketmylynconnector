package org.eclipse.mylyn.bitbucket.internal.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.mylyn.bitbucket.internal.BitbucketRepository;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Container of Bitbucket Issues
 * @author shuji.w6e
 * @since 0.1.0
 */
public class BBIssues extends BBCustomListItemModel {

    private final List<BBIssue> issues = new ArrayList<BBIssue>();
    
    private int size;
    private int page;
    private int pagelen;
    private String next;
    private String previous;
    private int count = 0;
    
    public int getPage() {
        return page;
    }

    public void setPage(int pagep) {
        this.page = pagep;
    }

    public int getPagelen() {
        return pagelen;
    }

    public void setPagelen(int pagelenp) {
        this.pagelen = pagelenp;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String nextp) {
        this.next = nextp;
    }

    public String getPrevious() {
        return previous;
    }

    public void setPrevious(String previousp) {
        this.previous = previousp;
    }

    public int getCount() {
        return count;
    }

    public BBIssue get(int idx) {
        return issues.get(idx);
    }
    
    public List<BBIssue> getIssues() {
        return issues;
    }
    
    public int addMoreIssues(List<BBIssue> moreIssues) {
        issues.addAll(moreIssues);
        return issues.size();
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append("BitbucketIssues[")
           .append("count=").append(count);
        if (!issues.isEmpty()) {
            Iterator<BBIssue> iter = issues.iterator();
            str.append(", issues=[")
               .append(iter.next().getLocalId());
            while (iter.hasNext()) {
                str.append(",")
                   .append(iter.next().getLocalId());
            }
            str.append("]");
        }
        str.append("]");
        return str.toString();
    }

    

    @Override
    public <T extends BBModelI> List<T> getList(){
        return null;
    }
    @Override
    public String buildUrl(BitbucketRepository bbr) {
        return BitbucketRepository.API_BITBUCKET + 
               BitbucketRepository.REPO_PART + 
               bbr.getUsername() + "/" + 
               bbr.getRepoSlug() + "/" + 
               "issues" + "/";
    }

    @Override
    public BBModelI interpretReturn(JSONObject object)  throws JSONException{
        size = JsonGet.getInt(object,"size");
        pagelen =JsonGet.getInt(object,"pagelen");
        page = JsonGet.getInt(object,"page");
        next = JsonGet.getString(object,"next");
        previous = JsonGet.getString(object,"previous");
        JSONArray  values = object.getJSONArray("values");
        for (int i=0;i<values.length();i++) {
            JSONObject issueObj = values.getJSONObject(i);
            BBIssue issue = new BBIssue();
            issue.interpretReturn(issueObj);
            issues.add(issue);
        }
        size = issues.size();
        return this;
    }

    @Override
    public String generateJson() {
        // TODO Auto-generated method stub
        return null;
    }

}
