package org.eclipse.mylyn.bitbucket.internal.model;

import org.json.JSONException;
import org.json.JSONObject;

public abstract class JsonGet {

    public static int getInt(JSONObject object,String key) {
        int result =0;
        if (object == null)
            return 0;
        try {
            result =  object.getInt(key);
        }
        catch (NullPointerException e){
            result = 0;
        }
        catch (ClassCastException e) {
            result=0;
        }
        catch (JSONException e) {
            result=0;
        }
       return result;
    }
    public static String getString(JSONObject object,String key) {
        String result ="";
        if (object == null)
            return "";
        try {
            result =  object.getString(key);
        }
        catch (NullPointerException e){
            result = "";
        }
        catch (ClassCastException e) {
            result="";
        }
        catch (JSONException e) {
            result="";
        }
       return result;
    }
    public static JSONObject getObject(JSONObject object,String key) {
        JSONObject result =null;
        if (object == null)
            return null;
        try {
            result =  object.getJSONObject(key);
        }
        catch (NullPointerException e){
            result = null;
        }
        catch (ClassCastException e) {
            result=null;
        }
        catch (JSONException e) {
            result=null;
        }
       return result;
    }

}
