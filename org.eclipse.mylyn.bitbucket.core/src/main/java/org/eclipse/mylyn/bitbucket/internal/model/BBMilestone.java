package org.eclipse.mylyn.bitbucket.internal.model;

import java.util.List;

import org.eclipse.mylyn.bitbucket.internal.BitbucketRepository;
import org.json.JSONObject;

public class BBMilestone implements BBModelI {
    
    private List<BBMilestone> listMilestones;

    private String name;
    
    private long id;
 
    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "BitbucketMilestone [name=" + name + ", id=" + id + "]";
    }


    @Override
    public String buildUrl(BitbucketRepository bbr) {
        return BitbucketRepository.API_BITBUCKET + BitbucketRepository.REPO_PART + bbr.getUsername() + "/" + bbr.getRepoSlug() + "/milestones";
    }

    @Override
    public String getKey() {
        return "" + id;
    }
    
    @Override
    public List<BBMilestone> getList(){
        return listMilestones;
    }
    @Override
    public BBMilestone interpretReturn(JSONObject object) {
        setName(JsonGet.getString(object, "name"));
        return this;
    }

    @Override
    public String generateJson() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getName() {
        return name;
    }

}
