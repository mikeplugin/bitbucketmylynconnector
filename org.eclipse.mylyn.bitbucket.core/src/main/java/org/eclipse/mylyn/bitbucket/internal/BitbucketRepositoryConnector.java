package org.eclipse.mylyn.bitbucket.internal;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.mylyn.bitbucket.internal.model.BBComment;
import org.eclipse.mylyn.bitbucket.internal.model.BBComments;
import org.eclipse.mylyn.bitbucket.internal.model.BBIssue;
import org.eclipse.mylyn.bitbucket.internal.model.BBIssues;
import org.eclipse.mylyn.bitbucket.internal.model.BBStatus;
import org.eclipse.mylyn.tasks.core.AbstractRepositoryConnector;
import org.eclipse.mylyn.tasks.core.IRepositoryQuery;
import org.eclipse.mylyn.tasks.core.ITask;
import org.eclipse.mylyn.tasks.core.TaskRepository;
import org.eclipse.mylyn.tasks.core.data.AbstractTaskDataHandler;
import org.eclipse.mylyn.tasks.core.data.TaskAttribute;
import org.eclipse.mylyn.tasks.core.data.TaskData;
import org.eclipse.mylyn.tasks.core.data.TaskDataCollector;
import org.eclipse.mylyn.tasks.core.data.TaskMapper;
import org.eclipse.mylyn.tasks.core.sync.ISynchronizationSession;

/**
 * Bitbucket connector.
 * @author shuji.w6e
 * @since 0.1.0
 */
public class BitbucketRepositoryConnector extends AbstractRepositoryConnector {

    /** Bitbucket kind. */
    protected static final String KIND = Bitbucket.CONNECTOR_KIND;
    /** Bitbucket specific {@link AbstractTaskDataHandler}. */
    private final BitbucketTaskDataHandler taskDataHandler;

    /**
     * Constructor for BitbucketRepositoryConnector.
     */
    public BitbucketRepositoryConnector() {
        this.taskDataHandler = new BitbucketTaskDataHandler();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getLabel() {
        return "Bitbucket (REST API v1)";
    }

    /**
     * {@inheritDoc}
     * @return "#"
     */
    @Override
    public String getTaskIdPrefix() {
        return "#";
    }

    /**
     * {@inheritDoc}
     * @see #KIND
     */
    @Override
    public String getConnectorKind() {
        return KIND;
    }

    
    /**
     * {@inheritDoc}
     */
    @Override
    public void updateRepositoryConfiguration(TaskRepository taskRepository, IProgressMonitor monitor)
            throws CoreException {
        // TODO do nothing?
    }

    /**
     * {@inheritDoc}
     * @return always {@code true}
     */
    @Override
    public boolean canCreateNewTask(TaskRepository repository) {
        return true;
    }

    /**
     * {@inheritDoc}
     * @return always {@code false}
     */
    @Override
    public boolean canCreateTaskFromKey(TaskRepository repository) {
        return false;
    }
    
    @Override
    public boolean canDeleteTask(TaskRepository repository, ITask task) {
        // TODO not support now
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getTaskUrl(String repositoryUrl, String taskId) {
        return BitbucketRepository.createFromUrl(repositoryUrl).getIssueUrl(taskId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getRepositoryUrlFromTaskUrl(String taskFullUrl) {
        return BitbucketRepository.createFromIssueUrl(taskFullUrl).getUrl();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getTaskIdFromTaskUrl(String taskFullUrl) {
        return BitbucketRepository.getIssueIdFromIssueUrl(taskFullUrl);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractTaskDataHandler getTaskDataHandler() {
        return this.taskDataHandler;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IStatus performQuery(TaskRepository repository, IRepositoryQuery query, TaskDataCollector collector,
            ISynchronizationSession session, IProgressMonitor monitor) {
        monitor.beginTask("Querying repository ...", 1);
        try {
            BBIssues issues = BitbucketService.get(repository).searchIssues(BitbucketQuery.get(query));
            // collect task data
            for (BBIssue issue : issues.getIssues()) {
                addCommentsToIssue(repository,issue);
                TaskData taskData = taskDataHandler.toTaskData(repository, issue);
                collector.accept(taskData);
            }
            return Status.OK_STATUS;
        } catch (BitbucketServiceException e) {
            return BitbucketStatus.newErrorStatus(e);
        } finally {
            monitor.done();
        }
    }

    @Override
    public TaskData getTaskData(TaskRepository repository, String taskId, IProgressMonitor monitor)
            throws CoreException {
        monitor.beginTask("Getting issue from repository ...", 1);
        try {
            BBIssue issue = new  BBIssue();
            issue.setLocalId(taskId);
            issue = BitbucketService.get(repository).doGet(issue);
            addCommentsToIssue(repository,issue);
            return taskDataHandler.toTaskData(repository, issue);
        } catch (BitbucketServiceException e) {
            throw new CoreException(BitbucketStatus.newErrorStatus(e));
        } finally {
            monitor.done();
        }
    }
    public void addCommentsToIssue(TaskRepository repository,BBIssue issue) throws BitbucketServiceException {        
        BBComments comments = BitbucketService.get(repository).doGetList(new BBComments(issue));
        for (BBComment comment : comments.getList()) {
            comment.setIssue(issue);
        }
        Collections.sort(comments.getList(),new Comparator<BBComment>() {
            @Override
            public int compare(BBComment o1, BBComment o2) {
                return o1.getUtcCreatedOn().compareTo(o2.getUtcCreatedOn());
            }
        });
        issue.setComments(comments.getList().toArray(new BBComment[comments.getSize()]));
    }
    @Override
    public boolean hasTaskChanged(TaskRepository repository, ITask task, TaskData taskData) {
        return new TaskMapper(taskData).hasChanges(task);
    }

    @Override
    public void updateTaskFromTaskData(TaskRepository taskRepository, ITask task, TaskData taskData) {
        if (!taskData.isNew()) {
            task.setUrl(getTaskUrl(taskRepository.getUrl(), taskData.getTaskId()));
        }
        new TaskMapper(taskData).applyTo(task);
        if (BBStatus.RESOLVED.toString().equalsIgnoreCase(taskData.getRoot().getMappedAttribute(TaskAttribute.STATUS).getValue())) {
            task.setCompletionDate(new Date());
        } else {
            task.setCompletionDate(null);
        }
    }
    
    

}
