package org.eclipse.mylyn.bitbucket.internal.model;

import java.util.List;

import org.eclipse.mylyn.bitbucket.internal.BitbucketRepository;
import org.json.JSONException;
import org.json.JSONObject;

public interface BBModelI {
    String name="";
    
    public String buildUrl(BitbucketRepository bbr);
    
    public String getKey();
    
    public <T extends BBModelI> List<T> getList();
    
    public BBModelI interpretReturn(JSONObject object) throws JSONException;
    public String generateJson();
    public String getName();
    

}
