package org.eclipse.mylyn.bitbucket.internal.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.List;

import org.eclipse.mylyn.bitbucket.internal.BitbucketRepository;
import org.json.JSONObject;


/**
 * Bitbucket Issue object.
 * @author shuji.w6e
 * @since 0.1.0
 */
public class BBIssue implements BBModelI {

    /** issue id */
    private String localId;
    private int id;
    /** issue title */
    private String title;
    /** issue content */
    private String content;
    private String markup;
    private String html;
    private String contentType;
    /** issue created on */
    private Date utcCreatedOn;
    /** */
    private Meta metadata = new Meta();
    
    private String status;
    
    private String priority;

    /** reporter */
    private BBUser reportedBy;
    
    private BBUser responsible;
    
    private long commentCount;
    
    private long followerCount;
    
    private BBComment[] comments = new BBComment[]{};;
    
    private String resourceUri;
    
    private boolean isSpam;
    
    public BBIssue() {}
    
    //respoinsible
    
    public BBIssue(String title, String content, String priority, String status, String kind) {
        super();
        this.title = title;
        this.content = content;
        this.priority = priority;
        this.status = status;
        this.metadata.setKind(kind);
    }

    public String getLocalId() {
        return localId;
    }

    public void setLocalId(String localId) {
        this.localId = localId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getUtcCreatedOn() {
        return utcCreatedOn;
    }

    public void setUtcCreatedOn(Date utcCreatedOn) {
        this.utcCreatedOn = utcCreatedOn;
    }

    public BBUser getReportedBy() {
        return reportedBy;
    }

    public void setReportedBy(BBUser reportedBy) {
        this.reportedBy = reportedBy;
    }

    public Meta getMetadata() {
        return metadata;
    }

    public void setMetadata(Meta metadata) {
        this.metadata = metadata;
    }

    public String getKind() {
        if (metadata == null) return null;
        return metadata.getKind();
    }

    public String getStatus() {
        return status;
    }
    
    public void setStatus(String status) {
        this.status = status;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }
    
    public BBUser getResponsible() {
        return responsible;
    }

    public void setResponsible(BBUser responsible) {
        this.responsible = responsible;
    }

    public long getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(long commentCount) {
        this.commentCount = commentCount;
    }

    public long getFollowerCount() {
        return followerCount;
    }

    public void setFollowerCount(long followerCount) {
        this.followerCount = followerCount;
    }

    public String getResourceUri() {
        return resourceUri;
    }

    public void setResourceUri(String resourceUri) {
        this.resourceUri = resourceUri;
    }

    public boolean isSpam() {
        return isSpam;
    }

    public void setSpam(boolean isSpam) {
        this.isSpam = isSpam;
    }
    public int getId() {
        return id;
    }

    public void setId(int idp) {
      id = idp;
    }

    public String getMarkup() {
        return markup;
    }

    public void setMarkup(String markupp) {
        markup = markupp;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String htmlp) {
        html = htmlp;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentTypep) {
        contentType = contentTypep;
    }



    @Override
    public String toString() {
        return "BitbucketIssue [localId=" + localId + ", title=" + title + ", content=" + content + ", utcCreatedOn="
                + utcCreatedOn + ", metadata=" + metadata + ", status=" + status + ", priority=" + priority
                + ", reportedBy=" + reportedBy + ", responsible=" + responsible + ", commentCount=" + commentCount
                + ", followerCount=" + followerCount + ", resourceUri=" + resourceUri + ", isSpam=" + isSpam + "]";
    }
    
    public static class Meta {
       
        private String kind="";
        private String milestone="";
        private String component="";
        private String version="";

        public String getKind() {
            return kind;
        }

        public void setKind(String kind) {
            this.kind = kind;
        }

        public String getMilestone() {
            return milestone;
        }

        public void setMilestone(String milestone) {
            this.milestone = milestone;
        }

        public String getComponent() {
            return component;
        }

        public void setComponent(String component) {
            this.component = component;
        }

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }

        @Override
        public String toString() {
            return "Meta [kind=" + kind + ", milestone=" + milestone + ", component=" + component + ", version="
                    + version + "]";
        }

    }


    @Override
    public String buildUrl(BitbucketRepository bbr) {
        return BitbucketRepository.API_BITBUCKET + 
               BitbucketRepository.REPO_PART + 
               bbr.getUsername() + "/" + 
               bbr.getRepoSlug() + "/" + 
               "issues" + "/";
    }
    
    @Override
    public String getKey() {
        return this.getLocalId();
    }

     @Override
    public <T extends BBModelI> List<T> getList(){
        return null;
    }

    public BBComment[] getComments() {
        return comments;
    }

    public void setComments(BBComment[] comments) {
        this.comments = comments;
    }

    @Override
    public BBModelI interpretReturn(JSONObject object) {
        priority = JsonGet.getString(object, "priority");
        title=JsonGet.getString(object, "title");
        try {
            utcCreatedOn = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(JsonGet.getString(object, "created_on"));
        }
        catch (ParseException  e) {
            utcCreatedOn = new Date(); 
        }
        catch (IllegalArgumentException e) {
            utcCreatedOn = new Date();             
        }
        status=JsonGet.getString(object, "state");
        id= JsonGet.getInt(object, "id");
        localId = String.valueOf(id);
        JSONObject contentObj = JsonGet.getObject(object,"content");
        content = JsonGet.getString(contentObj,"raw");
        markup = JsonGet.getString(contentObj,"markup");
        html = JsonGet.getString(contentObj,"html");
        contentType = JsonGet.getString(contentObj,"type");
        metadata.setKind(JsonGet.getString(object,"kind"));
        JSONObject componentObj = JsonGet.getObject(object,"component");
        if (componentObj != null)
            metadata.setComponent(JsonGet.getString(componentObj, "name"));
        JSONObject milestoneObj = JsonGet.getObject(object,"milestone");
        if (milestoneObj != null)
            metadata.setMilestone(JsonGet.getString(milestoneObj, "name"));
        JSONObject versionObj = JsonGet.getObject(object,"version");
        if (versionObj != null)
            metadata.setVersion(JsonGet.getString(versionObj, "name"));
        JSONObject userObj = JsonGet.getObject(object,"reporter");
        if (userObj != null) {
            reportedBy = new BBUser(JsonGet.getString(userObj, "display_name"));
            reportedBy.setAccountid(JsonGet.getString(userObj, "uuid"));
        }
        userObj = JsonGet.getObject(object,"assignee");
        if (userObj != null) {
            responsible= new BBUser(JsonGet.getString(userObj, "display_name"));
            responsible.setAccountid(JsonGet.getString(userObj, "uuid"));
        }
        return this;
    }

    @Override
    public String generateJson() {
        JSONObject object = new JSONObject();
        if (id != 0)
            object.put("id", id);
       object.put("title", title);
       if (utcCreatedOn == null)
           utcCreatedOn = new Date();
       Instant instant = utcCreatedOn.toInstant();
       object.put("created_on", instant.toString());
       object.put("state", status);
       object.put("kind", metadata.getKind());
       object.put("priority", priority);
       if (!metadata.getComponent().isEmpty()) {
           JSONObject componentObj = new JSONObject();
           componentObj.put("name",metadata.getComponent());
           object.put("component", componentObj);
       }
       if (!metadata.getMilestone().isEmpty()) {
           JSONObject milestoneObj = new JSONObject();
           milestoneObj.put("name",metadata.getMilestone());
           object.put("milestone", milestoneObj);
       }
       if (!metadata.getVersion().isEmpty()) {
           JSONObject versionObj = new JSONObject();
           versionObj.put("name",metadata.getVersion());
           object.put("version", versionObj);
       }
       if (!content.isEmpty()) {
           JSONObject contentObj = new JSONObject();
           contentObj.put("raw", content);
           contentObj.put("markup", "plaintext");
           object.put("content", contentObj);
       }
/*       if (reportedBy != null) {
           JSONObject reportedObj = new JSONObject();
           reportedObj.put("name",reportedBy.getUsername());
           if (reportedBy.getAccountid() != null)
               reportedObj.put("uuid",reportedBy.getAccountid());
           object.put("reporter", reportedObj);
       }
       if(responsible != null) {
           JSONObject responsibleObj = new JSONObject();
           responsibleObj.put("name",responsible.getUsername());
           if (responsible.getAccountid() != null)
               responsibleObj.put("uuid",responsible.getAccountid());
           object.put("assignee", responsibleObj);
       } */
       return object.toString();
    }

    @Override
    public String getName() {
        // TODO Auto-generated method stub
        return null;
    }

}
