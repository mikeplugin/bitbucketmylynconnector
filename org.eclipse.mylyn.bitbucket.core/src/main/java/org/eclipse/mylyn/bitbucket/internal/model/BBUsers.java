package org.eclipse.mylyn.bitbucket.internal.model;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.mylyn.bitbucket.internal.BitbucketRepository;
import org.json.JSONException;
import org.json.JSONObject;

public class BBUsers extends BBCustomListItemModel {
    private List<BBUser> listUsers;
 
    @Override
    public String buildUrl(BitbucketRepository bbr) {
        return BitbucketRepository.API_BITBUCKET +"user";
    }

    @Override
    public String getKey() {
        // TODO Auto-generated method stub
        return null;
    }


    @Override
    public List<BBUser> getList() {
        // TODO Auto-generated method stub
        return listUsers;
    }

    @Override
    public BBModelI interpretReturn(JSONObject object) throws JSONException {
        size = JsonGet.getInt(object,"size");
        pagelen =JsonGet.getInt(object,"pagelen");
        page = JsonGet.getInt(object,"page");
        next = JsonGet.getString(object,"next");
        previous = JsonGet.getString(object,"previous");
//        JSONArray  values = object.getJSONArray("values");
        listUsers =new ArrayList<BBUser>();
//        for (int i=0;i<values.length();i++) {
//            JSONObject objectTemp = values.getJSONObject(i);
            BBUser item = new BBUser();
            item.setUsername(JsonGet.getString(object, "display_name"));
            item.setAccountid(JsonGet.getString(object, "uuid"));
            listUsers.add(item);
 //       }
        return this;
    }

    @Override
    public String generateJson() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getName() {
        // TODO Auto-generated method stub
        return "Users";
    }

}
