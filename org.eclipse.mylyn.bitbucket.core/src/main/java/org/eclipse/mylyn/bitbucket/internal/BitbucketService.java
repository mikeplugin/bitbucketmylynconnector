package org.eclipse.mylyn.bitbucket.internal;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Enumeration;
import java.util.Random;
import java.util.jar.Manifest;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethodBase;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.URIException;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.DeleteMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.PutMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.httpclient.util.EncodingUtil;
import org.eclipse.core.net.proxy.IProxyData;
import org.eclipse.core.net.proxy.IProxyService;
import org.eclipse.mylyn.bitbucket.internal.model.BBCustomListItemModel;
import org.eclipse.mylyn.bitbucket.internal.model.BBIssues;
import org.eclipse.mylyn.bitbucket.internal.model.BBModelI;
import org.eclipse.mylyn.commons.net.WebUtil;
import org.eclipse.mylyn.tasks.core.TaskRepository;
import org.json.JSONException;
import org.json.JSONObject;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Service object to get/search/update issue of Bitbucket.
 * <p>
 * Bitbucket REST APIs documentation:
 * https://confluence.atlassian.com/display/BITBUCKET/Use+the+Bitbucket+REST+APIs
 * </p>
 * @since 0.1.0
 * @author shuji.w6e
 */
//TODO probably need caching in this - lots of calls to fetch milestones/versions/components/etc.
public class BitbucketService {
    
    public static BitbucketService get(TaskRepository repository) {
        return new BitbucketService(BitbucketRepository.createFromUrl(repository.getUrl()),
                BitbucketCredentials.create(repository));
    }

    public static BitbucketService get(String url, String username, String password) {
        return new BitbucketService(BitbucketRepository.createFromUrl(url),
                new BitbucketCredentials(username, password));
    }

    protected final BitbucketRepository repository;
    protected final BitbucketCredentials credentials;
    private static final ServiceTracker<IProxyService, IProxyService> proxyTracker;
    static {
      proxyTracker = new ServiceTracker<IProxyService, IProxyService>(FrameworkUtil.getBundle(BitbucketService.class).getBundleContext(), 
                                                                                                                IProxyService.class.getName(), null);
      proxyTracker.open();
    }

    /**
     * Constructor, create the client and JSON/Java interface object.
     */
    public BitbucketService(BitbucketRepository repository, BitbucketCredentials credentials) {
      this.repository = repository;
      this.credentials = credentials;
    }

    public boolean verifyCredentials() throws BitbucketServiceException {
      doGetWithRandomParameter(new BBIssues());
      return true;
    }

    public BBIssues searchIssues(BitbucketQuery query) throws BitbucketServiceException {
        String uri;
        try {
            uri = BitbucketRepository.API_BITBUCKET + BitbucketRepository.REPO_PART + repository.getUsername() + "/"
                    + repository.getRepoSlug() + "/" + "issues" + "/" + query.toQueryString(0);
            //System.err.println(uri);
        } catch (UnsupportedEncodingException e) {
            System.err.println("Exception? Really? query was " + query);
            e.printStackTrace();
            throw new BitbucketServiceException(e);
        }
        BBIssues issues = doGetIssuesQuery(uri);

        while (issues.getCount() > issues.getIssues().size()) {
            try {
                uri = BitbucketRepository.API_BITBUCKET + BitbucketRepository.REPO_PART + repository.getUsername()
                        + "/" + repository.getRepoSlug() + "/" + "issues" + "/"
                        + query.toQueryString(issues.getIssues().size());
            //System.err.println(uri);
            } catch (UnsupportedEncodingException e) {
                throw new BitbucketServiceException(e);
            }
            issues.addMoreIssues(doGetIssuesQuery(uri).getIssues());
        }
        return issues;
    }


    public <T> T doDelete(BBModelI model) throws BitbucketServiceException {
        String uri = model.buildUrl(repository) + model.getKey() + "/";
        //System.err.println("Calling uri: "  + uri);
        DeleteMethod method = new DeleteMethod(uri);
        return execute(method, credentials, model);
    }

    public <T extends BBCustomListItemModel> T doGetList(T model) throws BitbucketServiceException {
        String uri = ((BBModelI) model).buildUrl(repository);
        GetMethod method = new GetMethod(uri);
        T result = execute(method, credentials,model);
        return  result;
    }

    /**
     * Bitbucket REST API bug? GET requests seem to be cached on the 
     * server side and no HTTP header parameters make it behave. This 
     * method adds a random extra parameter to make the URL unique, 
     * thus avoiding the cache.
     */
    public <T> T doGetWithRandomParameter(BBModelI model) throws BitbucketServiceException {
        String uri = model.buildUrl(repository);
        uri += "?random=" + new Random().nextLong();
        //System.err.println("Calling uri: "  + uri);
        GetMethod method = new GetMethod(uri);
        return execute(method, credentials, model);
    }

    public <T> T doGet(BBModelI model) throws BitbucketServiceException {
        String uri = model.buildUrl(repository) + model.getKey();
        //System.err.println("Calling uri: "  + uri);
        GetMethod method = new GetMethod(uri);
        return execute(method, credentials, model);
    }

    public BBIssues doGetIssuesQuery(String uri) throws BitbucketServiceException {
        GetMethod method = new GetMethod(uri);
        return execute(method, credentials, new BBIssues());
    }

    /**
     * From https://confluence.atlassian.com/display/BITBUCKET/Use+the+Bitbucket+REST+APIs:
     * POST Creates new information.
     */
    public <T extends BBModelI> T doPost(T model) throws BitbucketServiceException {
        String uri = model.buildUrl(repository);
        //System.err.println("Calling uri: "  + uri);
        PostMethod method = new PostMethod(uri);
        StringRequestEntity data;
        try {
           data = new StringRequestEntity(model.generateJson(),"application/json","UTF-8");
        }
        catch (UnsupportedEncodingException e) {
            throw new BitbucketServiceException(e);
        }
        
        // We really should have task repository settings telling us what charcter set to use,
        // but Bitbucket seems to serve content in UTF-8, and so let's just suppose this is
        // OK until we get an issue reporting otherwise:
        //method.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        method.setRequestEntity(data);
        return execute(method, credentials, model);
    }

    /**
     * From https://confluence.atlassian.com/display/BITBUCKET/Use+the+Bitbucket+REST+APIs:
     * PUT Updates existing information.
     */
    public <T extends BBModelI> T doPut(T model) throws BitbucketServiceException {

        String uri = model.buildUrl(repository) + model.getKey() + "/";
        //System.err.println("Calling uri: "  + uri);
        PutMethod method = new PutMethod(uri);
        StringRequestEntity data;
        try {
          data = new StringRequestEntity(model.generateJson(),"application/json","UTF-8");        
        } catch (UnsupportedEncodingException e) {
          throw new BitbucketServiceException(e);
        }
        method.setRequestEntity(data);
        return execute(method, credentials, model);
    }

    private <T> T execute(HttpMethodBase method, BitbucketCredentials credentials, BBModelI model) throws BitbucketServiceException {
        
        HttpClient client = new HttpClient();

        try {
          for (IProxyData data : getProxyService().select(new URI(method.getURI().toString()))) {
            //if ((data.getHost() != null) && (data.getType().equalsIgnoreCase("https"))) {
            if ((data.getHost() != null) && (data.getType().equalsIgnoreCase(method.getURI().getScheme()))) {
              client.getHostConfiguration().setProxy(data.getHost(), data.getPort());
            }
          }
        } catch (URIException e1) {
          // TODO Auto-generated catch block
          e1.printStackTrace();
        } catch (URISyntaxException e1) {
          // TODO Auto-generated catch block
          e1.printStackTrace();
        }

        // Authentication
        if (credentials != null) {
            client.getParams().setAuthenticationPreemptive(true);
            client.getState().setCredentials(new AuthScope("api.bitbucket.org", 443, AuthScope.ANY_REALM),
                    new UsernamePasswordCredentials(credentials.getUsername(), credentials.getPassword()));
            method.setDoAuthentication(true);
        }
        
        method.addRequestHeader("User-Agent", WebUtil.getUserAgent(getUserAgent()));

        try {
            int statusCode = client.executeMethod(method);
            //System.err.print("Result "+method.getURI()+" returned "+statusCode+"\n");

            // TODO: distinguish 401 UNAUTHORIZED from 403 FORBIDDEN
            //       (see https://confluence.atlassian.com/display/BITBUCKET/Version+1)
            if (statusCode == HttpStatus.SC_UNAUTHORIZED || statusCode == HttpStatus.SC_FORBIDDEN) {
                // Retry with basic authorization
                String token = credentials.getUsername() + ":" + credentials.getPassword();
                method.setRequestHeader("Authorization", "Basic " + EncodingUtil.getAsciiString(Base64.encodeBase64(token.getBytes())));
                statusCode = client.executeMethod(method);
            }

            if (statusCode == HttpStatus.SC_UNAUTHORIZED || statusCode == HttpStatus.SC_FORBIDDEN) {
                // re-trying authentication obviously didn't succeed. Let's admit it.
                // (thus avoiding a null object further down and supplying a meaningful message to the user).
                /* BitbucketRepository rep = BitbucketRepository.createFromIssueUrl(method.getURI().toString()); */
                throw new BitbucketAuthorizationException( /* rep.getRepoSlug() + " " + */
                        "Repository reports an authorization problem. Are all credentials provided correctly?");
            }

            if (statusCode != HttpStatus.SC_OK
                    && (statusCode != HttpStatus.SC_NO_CONTENT && method instanceof DeleteMethod)) {
                throw new BitbucketServiceException(method.getURI() + " : " + method.getStatusText());
            }

            if (statusCode == HttpStatus.SC_BAD_REQUEST) {
                // Bitbucket returns HTML and a 400 error if we send invalid field values
                throw new BitbucketServiceException("Server reports invalid values. Please check all editable fields.");
            }

            if (statusCode == HttpStatus.SC_NOT_FOUND) {
                throw new BitbucketServiceException(
                        "Could not find item, it is possible that the repositories are not in sync. Please try synchronizing your repository");
            }
            // System.err.print(method.getResponseBodyAsString());
           String returned = method.getResponseBodyAsString();
            JSONObject object = new JSONObject(returned);
            return (T)model.interpretReturn(object);

        } catch (HttpException e) {
            throw new BitbucketServiceException(e);
        } catch (IOException e) {
            throw new BitbucketServiceException(e);
        } catch (JSONException e){
            throw new BitbucketServiceException(e);         
        }finally {
            method.releaseConnection();
        }
    }

    /** Static field to hold our part of the user-agent string. */
    private static String useragent = null;
    /**
     * Would be more elegant if this was set statically, but we're
     * not able to access the Manifest in a static context (or does
     * anyone know how?). This method should only be used once, since
     * the {@link #useragent} field is static. See usage in
     * {@link #execute(HttpMethodBase, BitbucketCredentials, Type)}.
     */
    private String getUserAgent() {
      if (useragent != null) {
        return useragent;
      }
      Manifest manifest = null;
      String useragent = "Bitbucket-Connector";
      try {
        Enumeration<URL> resources = getClass().getClassLoader().getResources("META-INF/MANIFEST.MF");
        while (resources.hasMoreElements()) {
          Manifest m = new Manifest(resources.nextElement().openStream());
          String name = m.getMainAttributes().getValue("Bundle-SymbolicName"); 
          if (name != null && name.equals("org.eclipse.mylyn.bitbucket.core")) {
            manifest = m;
            break;
          }
        }
      } catch (Exception E) {
      }
      if (manifest != null) {
        String v = manifest.getMainAttributes().getValue("Bundle-Version");
        if (v != null && (v.indexOf(".qualifier")>0)) {
          useragent += "/" + v.substring(0, v.indexOf(".qualifier"));
        }
      }
      return useragent;
    }

  /**
   * Trying the procedures found on http://www.snip2code.com/Snippet/427/How-to-retrieve-proxy-settings-within-Ec
   * to get access to proxy servers without getting warnings about discouraged access
   * (seems to be something about the {@link org.eclipse.core.internal.net.ProxySelector} class - 
   * Eclipse keeps warning us even for the above link in a javadoc comment!).
   */
  private static IProxyService getProxyService() {
    return proxyTracker.getService();
  }
}
