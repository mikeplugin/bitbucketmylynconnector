package org.eclipse.mylyn.bitbucket.internal.model;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.mylyn.bitbucket.internal.BitbucketRepository;
import org.json.JSONArray;
import org.json.JSONObject;

public class BBVersions extends BBCustomListItemModel {
    
   private List<BBVersion> listVersions;
    
    private Integer id;

 
    public String buildUrl(BitbucketRepository bbr) {
        return BitbucketRepository.API_BITBUCKET + BitbucketRepository.REPO_PART + bbr.getUsername() + "/" + bbr.getRepoSlug() + "/versions";
    }
    @Override
    public List<BBVersion> getList(){
        return listVersions;
    }

    @Override
    public BBModelI interpretReturn(JSONObject object) {
        size = JsonGet.getInt(object,"size");
        pagelen =JsonGet.getInt(object,"pagelen");
        page = JsonGet.getInt(object,"page");
        next = JsonGet.getString(object,"next");
        previous = JsonGet.getString(object,"previous");
        JSONArray  values = object.getJSONArray("values");
        listVersions =new ArrayList<BBVersion>();
        for (int i=0;i<values.length();i++) {
            JSONObject objectTemp = values.getJSONObject(i);
            BBVersion item = new BBVersion();
            item.interpretReturn(objectTemp);
            listVersions.add(item);
        }
        
      return this;
    }

    @Override
    public String generateJson() {
        // TODO Auto-generated method stub
        return null;
    }
    
}
