package org.eclipse.mylyn.bitbucket.internal.model;

public enum BBPriority {

    TRIVIAL("trivial"),
    MINOR("minor"),
    MAJOR("major"),
    CRITICAL("critical"),
    BLOCKER("blocker");
      
    
    BBPriority(final String priority) {
        this.priority = priority;
    }
    
    private final String priority;

    public String getPriority() {
        return priority;
    }

    public static String[] asArray() {
        return new String[]{TRIVIAL.getPriority(), MINOR.getPriority(), MAJOR.getPriority(), CRITICAL.getPriority(), BLOCKER.getPriority()};
    }
}
