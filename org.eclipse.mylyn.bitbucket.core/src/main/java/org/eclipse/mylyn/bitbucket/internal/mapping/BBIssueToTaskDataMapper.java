package org.eclipse.mylyn.bitbucket.internal.mapping;

import org.eclipse.mylyn.bitbucket.internal.model.BBIssue;
import org.eclipse.mylyn.tasks.core.TaskRepository;
import org.eclipse.mylyn.tasks.core.data.TaskData;

/**
 * Used to map part of the variables in BBIssue to their 
 * appropriate counterpart attribute in Mylyn's TaskData. 
 * 
 * Each implementing Mapper handles a different section of a BBIssue.
 */
public interface BBIssueToTaskDataMapper {

    /**
     * populates the provided TaskData with empty attributes - creating a new Issue Editor. 
     */
    public abstract void addAttributesToTaskData(TaskData data,TaskRepository repository);
    /**
     * populates the TaskData with attributes taken from the BBIssue object provided
     */
    public abstract void applyToTaskData(BBIssue issue, TaskData data, TaskRepository repository);
    /**
     * populates the provided BBIssue from the TaskData. 
     */
    public abstract void applyToIssue(TaskData data, BBIssue issue);
    
    /**
     * Check whether the TaskData isValid to update in Bitbucket in regards to the section this mapper handles.
     */
    public abstract boolean isValid(TaskData data);

}