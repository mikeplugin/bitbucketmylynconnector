package org.eclipse.mylyn.bitbucket.internal.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.eclipse.mylyn.bitbucket.internal.BitbucketRepository;
import org.json.JSONObject;


public class BBComment implements BBModelI{

    private String content= "";
    private String commentId;
    private Date utcUpdatedOn;
    private Date utcCreatedOn;
    private BBUser authorInfo;
    private BBIssue issue;

    public BBComment(BBIssue issue) {
        this.issue = issue;
    }
    public BBComment()
    {
        issue = null;
    }
    public BBComment(BBIssue issue,String content) {
        this.content = content;
        this.issue = issue;
    }
    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }

    public String getCommentId() {
        return commentId;
    }
    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }
    public Date getUtcUpdatedOn() {
        return utcUpdatedOn;
    }
    public void setUtcUpdatedOn(Date utcUpdatedOn) {
        this.utcUpdatedOn = utcUpdatedOn;
    }
    public Date getUtcCreatedOn() {
        return utcCreatedOn;
    }
    public void setUtcCreatedOn(Date utcCreatedOn) {
        this.utcCreatedOn = utcCreatedOn;
    }
 
    @Override
    public String buildUrl(BitbucketRepository bbr) {
        return BitbucketRepository.API_BITBUCKET + BitbucketRepository.REPO_PART + bbr.getUsername() + "/" + bbr.getRepoSlug() + "/issues/" + issue.getLocalId() + "/comments/";
    }
    @Override
    public String getKey() {
        return getCommentId();
    }
    @Override
    public <T extends BBModelI> List<T> getList(){
        return null;
    }
    public BBIssue getIssue() {
        return issue;
    }
    public void setIssue(BBIssue issue) {
        this.issue = issue;
    }
    public BBUser getAuthorInfo() {
        return authorInfo;
    }
    public void setAuthorInfo(BBUser authorInfo) {
        this.authorInfo = authorInfo;
    }
    @Override
    public BBModelI interpretReturn(JSONObject object) {
        int idTemp =JsonGet.getInt(object,"id");
        setCommentId(String.valueOf(idTemp));
        try {
            utcCreatedOn = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(JsonGet.getString(object, "created_on"));
        }
        catch (ParseException  e) {
            utcCreatedOn = new Date(); 
        }
        catch (IllegalArgumentException e) {
            utcCreatedOn = new Date();             
        }
        try {
            utcUpdatedOn = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(JsonGet.getString(object, "updated_on"));
        }
        catch (ParseException  e) {
            utcCreatedOn = new Date(); 
        }
        catch (IllegalArgumentException e) {
            utcCreatedOn = new Date();             
        }
        JSONObject authorObj = JsonGet.getObject(object, "user");
        if (authorObj != null) {
            authorInfo = new BBUser();
            authorInfo.interpretReturn(authorObj);
        }
        JSONObject contentObj = JsonGet.getObject(object,"content");
        if (contentObj != null) {
            setContent(JsonGet.getString(contentObj, "raw"));
        }

        return this;
    }
    @Override
    public String generateJson() {
        JSONObject object = new JSONObject();
        JSONObject contentObj = new JSONObject();
        contentObj.put("raw", content);
        contentObj.put("markup", "plaintext");
        object.put("content", contentObj);
        return object.toString();
    }
    @Override
    public String getName() {
        // TODO Auto-generated method stub
        return null;
    }
    
}
