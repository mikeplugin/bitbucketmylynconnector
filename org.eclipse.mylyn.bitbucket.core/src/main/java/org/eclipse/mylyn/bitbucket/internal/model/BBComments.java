package org.eclipse.mylyn.bitbucket.internal.model;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.mylyn.bitbucket.internal.BitbucketRepository;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class BBComments extends BBCustomListItemModel {
    private BBIssue issue;;
    private List<BBComment> listComments;
    public BBComments(BBIssue issuep) {
        issue = issuep;
    }

    @Override
    public String buildUrl(BitbucketRepository bbr) {
        return BitbucketRepository.API_BITBUCKET + BitbucketRepository.REPO_PART + bbr.getUsername() + "/" + bbr.getRepoSlug() + "/issues/" + issue.getLocalId() + "/comments/";
    }

    @Override
    public String getKey() {
        // TODO Auto-generated method stub
        return null;
    }


    @Override
    public List<BBComment> getList() {
        // TODO Auto-generated method stub
        return listComments;
    }

    @Override
    public BBModelI interpretReturn(JSONObject object) throws JSONException {
        size = JsonGet.getInt(object,"size");
        pagelen =JsonGet.getInt(object,"pagelen");
        page = JsonGet.getInt(object,"page");
        next = JsonGet.getString(object,"next");
        previous = JsonGet.getString(object,"previous");
        JSONArray  values = object.getJSONArray("values");
        listComments =new ArrayList<BBComment>();
        for (int i=0;i<values.length();i++) {
            JSONObject objectTemp = values.getJSONObject(i);
            BBComment item = new BBComment();
            item.interpretReturn(objectTemp);
            if (!item.getContent().isEmpty())  // ignoring empty comments generated by Bitbucket when status changed
                listComments.add(item);
       }
        size = listComments.size();
       return this;
    }

    @Override
    public String generateJson() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getName() {
        // TODO Auto-generated method stub
        return null;
    }

}
