package org.eclipse.mylyn.bitbucket.internal.mapping;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.mylyn.bitbucket.internal.BitbucketService;
import org.eclipse.mylyn.bitbucket.internal.BitbucketServiceException;
import org.eclipse.mylyn.bitbucket.internal.model.BBCustomListItemModel;
import org.eclipse.mylyn.bitbucket.internal.model.BBModelI;
import org.eclipse.mylyn.tasks.core.TaskRepository;

/**
 * an OptionProvider that retrieves it's values from BitBucket directly based on a provided model.
 */
public class BBGetListOptionProvider implements OptionProvider {

    private BBCustomListItemModel model;
    public BBGetListOptionProvider(BBCustomListItemModel model) {
        this.model = model;
    }
    @Override
    public Set<Entry<String, String>> getOptions(TaskRepository repository) {
        HashMap<String, String> options = new HashMap<String, String>();
        BitbucketService bbs = BitbucketService.get(repository);
        try {
            model = bbs.doGetList(model);
            for (BBModelI item : model.getList()) {
                options.put(item.getName(), item.getName());
            }
        }
        catch (BitbucketServiceException e) {
            options.put(model.getName(), model.getName());
            System.err.println("Got exception trying to get list for type=" + model.getClass());
        }
        return options.entrySet();
    }

}
