package org.eclipse.mylyn.bitbucket.internal.model;

import java.util.List;

import org.eclipse.mylyn.bitbucket.internal.BitbucketRepository;
import org.json.JSONException;
import org.json.JSONObject;

public class BBUser implements BBModelI{
    private String username;
    private String firstName;
    private String lastName;
    private boolean isTeam;
    private String email;
    private String avatar;
    private String resourceUri;
    private String accountid;
     

    public BBUser()  {}
    
    public BBUser(String username) {
        this.username = username;
    }
    
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public boolean isTeam() {
        return isTeam;
    }
    public void setTeam(boolean isTeam) {
        this.isTeam = isTeam;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getAvatar() {
        return avatar;
    }
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
    public String getResourceUri() {
        return resourceUri;
    }
    public void setResourceUri(String resourceUri) {
        this.resourceUri = resourceUri;
    }
    @Override
    public String toString() {
        return "User [username="    + username +
                   ", firstName="   + firstName +
                   ", lastName="    + lastName +
                   ", isTeam="      + isTeam +
                   ", email="       + email +
                   ", avatar="      + avatar +
                   ", resourceUri=" + resourceUri +
                "]";
    }

    /** Lets keep the method name similar to the BBModelI method name. */
    public static String buildUrl() {
        return "https://bitbucket.org/api/1.0/user/";
    }

    public String getAccountid() {
        return accountid;
    }

    public void setAccountid(String accountidp) {
        accountid = accountidp;
    }


    @Override
    public String buildUrl(BitbucketRepository bbr) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getKey() {
        // TODO Auto-generated method stub
        return null;
    }


    @Override
    public <T extends BBModelI> List<T> getList() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public BBModelI interpretReturn(JSONObject object) throws JSONException {
        setUsername(JsonGet.getString(object, "display_name"));
        setAccountid(JsonGet.getString(object, "uuid"));
        return this;
    }

    @Override
    public String generateJson() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getName() {
        // TODO Auto-generated method stub
        return null;
    }


    
}
