package org.eclipse.mylyn.bitbucket.internal;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.mylyn.bitbucket.internal.mapping.BBIssueToTaskDataMapper;
import org.eclipse.mylyn.bitbucket.internal.mapping.CommentDataMapper;
import org.eclipse.mylyn.bitbucket.internal.mapping.SimpleAttributeDataMapper;
import org.eclipse.mylyn.bitbucket.internal.model.BBComment;
import org.eclipse.mylyn.bitbucket.internal.model.BBIssue;
import org.eclipse.mylyn.tasks.core.ITaskMapping;
import org.eclipse.mylyn.tasks.core.RepositoryResponse;
import org.eclipse.mylyn.tasks.core.RepositoryResponse.ResponseKind;
import org.eclipse.mylyn.tasks.core.TaskRepository;
import org.eclipse.mylyn.tasks.core.data.AbstractTaskDataHandler;
import org.eclipse.mylyn.tasks.core.data.TaskAttribute;
import org.eclipse.mylyn.tasks.core.data.TaskAttributeMapper;
import org.eclipse.mylyn.tasks.core.data.TaskData;

/**
 * The taskDataHandler is the main entry point for the Editor to get access to BitBucket service.
 * It gets request to get and update Issues in BitBucket, and is responsible for mapping between the 
 * editor and the Issue.
 */
public class BitbucketTaskDataHandler extends AbstractTaskDataHandler {

    private static final String DATA_VERSION = "1";
    private List<BBIssueToTaskDataMapper> mappers = new ArrayList<BBIssueToTaskDataMapper>();
 
    public BitbucketTaskDataHandler() {
        mappers.add(new SimpleAttributeDataMapper());
        mappers.add(new CommentDataMapper());
    }

    @Override
    public TaskAttributeMapper getAttributeMapper(TaskRepository taskRepository) {
        return new TaskAttributeMapper(taskRepository);
    }

    @Override
    public boolean initializeTaskData(TaskRepository repository, TaskData data, ITaskMapping initializationData,
            IProgressMonitor monitor) throws CoreException {
      // TODO: We come here from TasksUiInternal.createTaskData.
      //       If we're not logged in, an exception is thrown.
      //       Instead, we should just return false ...
      try {
        BitbucketService.get(repository).verifyCredentials();
        data.setVersion(DATA_VERSION);
        for (BBIssueToTaskDataMapper mapper : mappers) {
          mapper.addAttributesToTaskData(data, repository);
        }
        return true;
      } catch (BitbucketAuthorizationException e) {
        throw new CoreException(BitbucketStatus.newErrorStatus(e));
      } catch (BitbucketServiceException e) {
        // What should this be?
        throw new CoreException(BitbucketStatus.newErrorStatus(e));
      }
    }
    
    // This populates from the server
    public TaskData toTaskData(TaskRepository repository, BBIssue issue) throws BitbucketServiceException {
        
        TaskData data = new TaskData(getAttributeMapper(repository), BitbucketRepositoryConnector.KIND,
                repository.getRepositoryUrl(), issue.getLocalId());
        data.setVersion(DATA_VERSION);
        for (BBIssueToTaskDataMapper mapper : mappers) {
            mapper.applyToTaskData(issue, data, repository);
        }
        addNewCommentSection(data);
        data.setPartial(hasNullValueInRequiredAttributes(data));
        
        return data;
    }
    private void addNewCommentSection(TaskData data) {

        data.getRoot().createAttribute(TaskAttribute.COMMENT_NEW).getMetaData()
            .setType(TaskAttribute.TYPE_LONG_RICH_TEXT).setReadOnly(false);        
    }
    
    // return true if data has null value of required attribute 
    private boolean hasNullValueInRequiredAttributes(TaskData data) {
        for (BBIssueToTaskDataMapper mapper : mappers) {
            if (!mapper.isValid(data)) return true;
        }
        return false;
    }

    @Override
    public RepositoryResponse postTaskData(TaskRepository repository, TaskData taskData,
            Set<TaskAttribute> oldAttributes, IProgressMonitor monitor) throws CoreException {
        try {
            BBIssue issue = new BBIssue();
            for (BBIssueToTaskDataMapper mapper : mappers) {
                mapper.applyToIssue(taskData, issue);
            }
            BBIssue newIssue = null;
            if (taskData.isNew()) {
            	newIssue = BitbucketService.get(repository).doPost(issue);
            } else {
                // TODO handle Operation
            	newIssue = BitbucketService.get(repository).doPut(issue);
            	TaskAttribute newCommentAttribute = taskData.getRoot().getMappedAttribute(TaskAttribute.COMMENT_NEW);
            	if (newCommentAttribute != null && StringUtils.isNotBlank(newCommentAttribute.getValue())) {
            	    BBComment comment = new BBComment(newIssue,newCommentAttribute.getValue());
            	    BitbucketService.get(repository).doPost(comment);
            	}
            }
            ResponseKind responseKind = taskData.isNew() ? ResponseKind.TASK_CREATED : ResponseKind.TASK_UPDATED;
            return new RepositoryResponse(responseKind, newIssue.getLocalId());
        } catch (BitbucketServiceException e) {
            throw new CoreException(BitbucketStatus.newErrorStatus(e));
        } catch (NullPointerException e) {
            throw new CoreException(BitbucketStatus.newErrorStatus("Please check your login credentials", e));
        }
    }

}
