package org.eclipse.mylyn.bitbucket.internal.model;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.mylyn.bitbucket.internal.BitbucketRepository;
import org.json.JSONArray;
import org.json.JSONObject;

public class BBMilestones extends BBCustomListItemModel {
    private List<BBMilestone> listMilestones;

    private String name;
    
    private long id;

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "BitbucketMilestone [name=" + name + ", id=" + id + "]";
    }

 
    @Override
    public String buildUrl(BitbucketRepository bbr) {
        return BitbucketRepository.API_BITBUCKET + BitbucketRepository.REPO_PART + bbr.getUsername() + "/" + bbr.getRepoSlug() + "/milestones";
    }

    @Override
    public String getKey() {
        return "" + id;
    }
    @Override
    public List<BBMilestone> getList(){
        return listMilestones;
    }
    @Override
    public BBMilestones interpretReturn(JSONObject object) {
        size = JsonGet.getInt(object,"size");
        pagelen =JsonGet.getInt(object,"pagelen");
        page = JsonGet.getInt(object,"page");
        next = JsonGet.getString(object,"next");
        previous = JsonGet.getString(object,"previous");
        JSONArray  values = object.getJSONArray("values");
        listMilestones =new ArrayList<BBMilestone>();
        for (int i=0;i<values.length();i++) {
            JSONObject objectTemp = values.getJSONObject(i);
            BBMilestone item = new BBMilestone();
            item.interpretReturn(objectTemp);
            listMilestones.add(item);
        }
        return this;
    }

    @Override
    public String generateJson() {
        // TODO Auto-generated method stub
        return null;
    }
}
