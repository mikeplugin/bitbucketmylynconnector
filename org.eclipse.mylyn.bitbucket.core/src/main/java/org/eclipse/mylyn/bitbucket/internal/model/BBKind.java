package org.eclipse.mylyn.bitbucket.internal.model;

public enum BBKind {
    
    BUG("bug"),
    ENHANCEMENT("enhancement"),
    PROPOSAL("proposal"),
    TASK("task");
    
    BBKind(final String kind) {
        this.kind = kind;
    }
    
    private final String kind;

    public String getKind() {
        return kind;
    }
    
    public static String[] asArray() {
        return new String[]{BUG.getKind(), ENHANCEMENT.getKind(), PROPOSAL.getKind(), TASK.getKind()};
    }

}
