package org.eclipse.mylyn.bitbucket.internal.model;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.mylyn.bitbucket.internal.BitbucketRepository;
import org.json.JSONArray;
import org.json.JSONObject;

public class BBComponents extends BBCustomListItemModel {
  private List<BBComponent> listComponents;

    private String name;
    
    private long id;

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "BitbucketComponent [name=" + name + ", id=" + id + "]";
    }


    public String buildUrl(BitbucketRepository bbr) {
        return BitbucketRepository.API_BITBUCKET + BitbucketRepository.REPO_PART + bbr.getUsername() + "/" + bbr.getRepoSlug() + "/components/";
    }

    @Override
    public String getKey() {
        return "" + id;
    }
    
     @Override
    public List<BBComponent> getList(){
        return listComponents;
    }
    @Override
    public BBModelI interpretReturn(JSONObject object) {
        size = JsonGet.getInt(object,"size");
        pagelen =JsonGet.getInt(object,"pagelen");
        page = JsonGet.getInt(object,"page");
        next = JsonGet.getString(object,"next");
        previous = JsonGet.getString(object,"previous");
        JSONArray  values = object.getJSONArray("values");
        listComponents =new ArrayList<BBComponent>();
        for (int i=0;i<values.length();i++) {
            JSONObject objectTemp = values.getJSONObject(i);
            BBComponent item = new BBComponent();
            item.interpretReturn(objectTemp);
            listComponents.add(item);
        }
        return this;
    }

    @Override
    public String generateJson() {
        // TODO Auto-generated method stub
        return null;
    }

}
