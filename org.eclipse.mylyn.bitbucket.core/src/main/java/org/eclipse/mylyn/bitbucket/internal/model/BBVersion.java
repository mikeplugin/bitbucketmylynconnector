package org.eclipse.mylyn.bitbucket.internal.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.mylyn.bitbucket.internal.BitbucketRepository;
import org.json.JSONObject;

public class BBVersion implements BBModelI {
    
    private String name;
    private List<BBVersion> listVersions;
    
    private Integer id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String toString() {
        return "BitbucketVersion [name=" + name + ", id=" + id + "]";
    }

    public Map<String, String> getParams() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("name", name);
        return map;
    }

    public String buildUrl(BitbucketRepository bbr) {
        return BitbucketRepository.API_BITBUCKET + BitbucketRepository.REPO_PART + bbr.getUsername() + "/" + bbr.getRepoSlug() + "/versions";
    }

    public String getKey() {
        return "" + getId();
    }
   @Override
    public List<BBVersion> getList(){
        return listVersions;
    }

    @Override
    public BBModelI interpretReturn(JSONObject object) {
           setName(JsonGet.getString(object, "name"));
      return this;
    }

    @Override
    public String generateJson() {
        // TODO Auto-generated method stub
        return null;
    }
    
}
