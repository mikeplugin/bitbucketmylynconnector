package org.eclipse.mylyn.bitbucket.internal.model;

import java.util.List;

import org.eclipse.mylyn.bitbucket.internal.BitbucketRepository;
import org.json.JSONObject;


public class BBComponent implements BBModelI {
    
    private List<BBComponent> listComponents;

    private String name;
    
    private long id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "BitbucketComponent [name=" + name + ", id=" + id + "]";
    }


    public String buildUrl(BitbucketRepository bbr) {
        return BitbucketRepository.API_BITBUCKET + BitbucketRepository.REPO_PART + bbr.getUsername() + "/" + bbr.getRepoSlug() + "/components/";
    }

    @Override
    public String getKey() {
        return "" + id;
    }
    
    @Override
    public List<BBComponent> getList(){
        return listComponents;
    }
    @Override
    public BBModelI interpretReturn(JSONObject object) {
            setName(JsonGet.getString(object, "name"));
        return this;
    }

    @Override
    public String generateJson() {
        // TODO Auto-generated method stub
        return null;
    }

}
