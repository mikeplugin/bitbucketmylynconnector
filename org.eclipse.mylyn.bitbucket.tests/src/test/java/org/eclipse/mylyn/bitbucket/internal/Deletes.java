package org.eclipse.mylyn.bitbucket.internal;

import java.util.List;
import java.util.logging.Logger;

import org.eclipse.mylyn.bitbucket.internal.model.BBComponent;
import org.eclipse.mylyn.bitbucket.internal.model.BBComponents;
import org.eclipse.mylyn.bitbucket.internal.model.BBIssue;
import org.eclipse.mylyn.bitbucket.internal.model.BBIssues;
import org.eclipse.mylyn.bitbucket.internal.model.BBMilestone;
import org.eclipse.mylyn.bitbucket.internal.model.BBMilestones;
import org.eclipse.mylyn.bitbucket.internal.model.BBVersion;
import org.eclipse.mylyn.bitbucket.internal.model.BBVersions;
import org.junit.Test;

public class Deletes {
    
    private final static Logger LOGGER = Logger.getLogger(Deletes.class.getName());
    private static final BitbucketService bbc = BitbucketService.get(TestConstants.TEST_URL, TestConstants.TEST_USERNAME, TestConstants.TEST_PASSWORD);
    
    @Test
    public void deleteVersions() throws Exception {
        BBVersions bbvs = bbc.doGetList(new BBVersions());
        for (BBVersion bbv : bbvs.getList()) {
            bbc.doDelete(bbv);
        }
    }
        
    @Test
    public void deleteMilestones() throws Exception {
        BBMilestones bbms = bbc.doGetList(new BBMilestones());
        for (BBMilestone bbv : bbms.getList()) {
            bbc.doDelete(bbv);
        }
    }
        
    @Test
    public void deleteComponents() throws Exception {
        BBComponents bbcs = bbc.doGetList(new BBComponents());
        for (BBComponent bbv : bbcs.getList()) {
            bbc.doDelete(bbv);
        }
    }
        
    @Test
    public void deleteIssues() throws Exception {
        BBIssues bbis = bbc.doGet(new BBIssues());
        for (BBIssue bbv : bbis.getIssues()) {
            bbc.doDelete(bbv);
        }
        LOGGER.info("Issue tracker cleared");
    }

}
