package org.eclipse.mylyn.bitbucket.internal;

import org.eclipse.mylyn.bitbucket.internal.model.BBVersion;
import org.eclipse.mylyn.bitbucket.internal.model.BBVersions;
import org.junit.Test;

public class Versions {

    
    @Test
    public void crudVersions() throws Exception {
        BitbucketService bbc = BitbucketService.get(TestConstants.TEST_URL, TestConstants.TEST_USERNAME, TestConstants.TEST_PASSWORD);
        
        //create
        BBVersion m = new BBVersion();
        m.setName("test version");
        m = bbc.doPost(m);
        
        //update
        m.setName("new updated version we set");
        bbc.doPut(m);
        
        //retrieve
        bbc.doGet(m);
        
        //retrieve as Collection (different to the rest as it has a wrapper type not just enclosing json array
        for (BBVersion bbm : bbc.doGetList(new BBVersions()).getList()) {
            System.err.println(bbm);
        }
        
        //delete
        bbc.doDelete(m);
        
    }    
    
}
